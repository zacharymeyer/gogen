# gogen

[![pipeline status](https://gitlab.com/zacharymeyer/gogen/badges/main/pipeline.svg)](https://gitlab.com/zacharymeyer/gogen/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

A 2d procedural level generator framework for Golang. This project is being built along side my simple [roguelike](https://gitlab.com/zacharymeyer/roguelike) game following the [r/roguelikedev](https://reddit.com/r/roguelikedev) tutorial.

### TODO:

- Generate rooms
- Generate level

### Credits:

- [Roguelikedev Tutorial](https://rogueliketutorials.com/)
- [Office Struggle](https://github.com/jmaister/officestruggle)
- [Gohan ECS Framework](https://code.rocketnine.space/tslocum/gohan)
