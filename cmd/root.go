/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/zacharymeyer/gogen/pkg/dungeon"
	"gitlab.com/zacharymeyer/gogen/pkg/grid"
)

var posX, posY, width, height int
var maxRooms, minRoomSize, maxRoomSize int

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gogen",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		pt := grid.Point{X: posX, Y: posY}
		opts := dungeon.RoomOptions{MaxRoomCount: uint(maxRooms), MinSize: uint(minRoomSize), MaxSize: uint(maxRoomSize)}
		level := dungeon.NewLevel(pt, width, height, opts)

		if level != nil {
		  center := level.Center
			fmt.Printf("level: length - %d; center: %d, %d\n", len(level.Rooms), center.X, center.Y)

			for i, room := range level.Rooms {
				fmt.Printf("room[%d]: tile length - %d; start - %d, %d; end - %d, %d\n", i, len(room.Tiles), room.Rect.X1, room.Rect.Y1, room.Rect.X2, room.Rect.Y2)
			}
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.gogen.yaml)")
	rootCmd.PersistentFlags().IntVar(&posX, "x", 0, "Position X (default is 0)")
	rootCmd.PersistentFlags().IntVar(&posY, "y", 0, "Position Y (default is 0)")
	rootCmd.PersistentFlags().IntVar(&width, "width", 20, "Level Width (default is 20)")
	rootCmd.PersistentFlags().IntVar(&height, "height", 20, "Level Height (default is 20)")
	rootCmd.PersistentFlags().IntVar(&maxRooms, "rooms", 30, "Maximum rooms (default is 30)")
	rootCmd.PersistentFlags().IntVar(&minRoomSize, "min", 6, "Minimum room size (default is 6)")
	rootCmd.PersistentFlags().IntVar(&maxRoomSize, "max", 10, "Maximum room size (default is 10)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
