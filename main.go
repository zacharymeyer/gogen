/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package main

import "gitlab.com/zacharymeyer/gogen/cmd"

func main() {
	cmd.Execute()
}
