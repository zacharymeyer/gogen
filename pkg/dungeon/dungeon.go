/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package dungeon

import (
	"gitlab.com/zacharymeyer/gogen/pkg/grid"
	"gitlab.com/zacharymeyer/gogen/pkg/utils"
)

type (
	TileType uint8

	Tile struct {
		Point    grid.Point
		TileType TileType
	}

	Room struct {
		Tiles []*Tile
		Rect  *grid.Rectangle
	}

	RoomOptions struct {
		MaxRoomCount     uint
		MinSize, MaxSize uint
	}

	Level struct {
		Rooms  []*Room
		Center grid.Point
	}
)

const (
	WALL TileType = iota
	FLOOR
)

func NewTile(pt grid.Point, tileType TileType) *Tile {
	return &Tile{
		Point:    pt,
		TileType: tileType,
	}
}

func NewRoom(pt1 grid.Point, width, height int) *Room {
	rect := grid.NewRectangle(pt1.X, pt1.Y, width, height)
	tiles := make([]*Tile, 0)

	for i := rect.Y1; i <= rect.Y2; i++ {
		for j := rect.X1; j <= rect.X2; j++ {
			tileType := FLOOR
			if i == rect.Y1 || i == rect.Y2 || j == rect.X1 || j == rect.X2 {
				tileType = WALL
			}
			tile := NewTile(grid.Point{X: j, Y: i}, tileType)
			tiles = append(tiles, tile)
		}
	}

	return &Room{
		Tiles: tiles,
		Rect:  rect,
	}
}

func NewLevel(pt grid.Point, width, height int, opts RoomOptions) *Level {
	roomCount := opts.MaxRoomCount
	minSize, maxSize := opts.MinSize, opts.MaxSize
	rooms := make([]*Room, 0)

	for i := 0; i < int(roomCount); i++ {
		randW, randH := utils.GetRandomUInt(minSize, maxSize), utils.GetRandomUInt(minSize, maxSize)
		randX, randY := utils.GetRandomUInt(uint(pt.X), uint(width)-randW-1), utils.GetRandomUInt(uint(pt.Y), uint(height)-randH-1)

		newRoom := NewRoom(grid.Point{X: int(randX), Y: int(randY)}, int(randW), int(randH))

		intersects := false
		for _, room := range rooms {
			if room.Rect.Intersects(newRoom.Rect) {
				intersects = true
				break
			}
		}

		if !intersects {
			rooms = append(rooms, newRoom)
		}
	}

	return &Level{
		Rooms:  rooms,
		Center: rooms[0].Rect.Center(),
	}
}
