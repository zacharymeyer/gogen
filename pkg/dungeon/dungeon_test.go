/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package dungeon

import (
	"testing"

	"gitlab.com/zacharymeyer/gogen/pkg/grid"
)

func TestNewTile(t *testing.T) {
	tile := NewTile(grid.Point{X: 10, Y: 15}, WALL)
	if tile != nil {
		pt := tile.Point
		if pt.X != 10 && pt.Y != 15 {
			t.Errorf("pt == %d, %d; expected 10, 15", pt.X, pt.Y)
		}
		if tile.TileType != WALL {
			t.Errorf("TileType == %d; expected WALL", tile.TileType)
		}
	} else {
		t.Error("tile == nil")
	}
}

func TestNewRoom(t *testing.T) {
	pt1, pt2 := grid.Point{X: 0, Y: 0}, grid.Point{X: 9, Y: 9}
	room := NewRoom(pt1, pt2.X, pt2.Y)
	if room != nil {
		tiles := room.Tiles
		tileLength := len(tiles)
		if tileLength != 100 {
			t.Errorf("tileLength == %d; expected 100", tileLength)
		}

		for i := 0; i < tileLength; i++ {
			if i < 11 && tiles[i].TileType != WALL {
				t.Errorf("[%d]: tile.TileType == %d; expected WALL", i, tiles[i].TileType)
			} else if i > 10 && i < 19 && tiles[i].TileType != FLOOR {
				t.Errorf("[%d]: tile.TileType == %d; expected FLOOR", i, tiles[i].TileType)
			} else if i > 20 && i < 29 && tiles[i].TileType != FLOOR {
				t.Errorf("[%d]: tile.TileType == %d; expected FLOOR", i, tiles[i].TileType)
			}
		}
	} else {
		t.Error("room == nil")
	}
}

func TestNewLevel(t *testing.T) {
	levelPt := grid.Point{X: 0, Y: 0}
	opts := RoomOptions{
		MaxRoomCount: 30,
		MinSize:      6,
		MaxSize:      10,
	}
	level := NewLevel(levelPt, 99, 99, opts)

	if level != nil {
		rooms := level.Rooms
		if len(rooms) < 1 {
			t.Errorf("len(rooms) == %d; expected at least 1", len(rooms))
		}
		center := level.Center
		if center.X < 1 || center.X > 99 || center.Y < 1 || center.Y > 99 {
			t.Errorf("center == %d, %d; expected x, y to be < 0 and >= 99", center.X, center.Y)
		}
	} else {
		t.Errorf("level == nil")
	}
}
