/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package grid

import "math"

type (
	Point struct {
		X, Y int
	}

	Rectangle struct {
		X1, Y1 int
		X2, Y2 int
	}
)

func NewRectangle(x, y, width, height int) *Rectangle {
	return &Rectangle{
		X1: x,
		Y1: y,
		X2: x + width,
		Y2: y + height,
	}
}

func (r *Rectangle) Center() Point {
	return Point{
		X: int(math.Round(float64(r.X1+r.X2) / 2.0)),
		Y: int(math.Round(float64(r.Y1+r.Y2) / 2.0)),
	}
}

func (r *Rectangle) Intersects(r2 *Rectangle) bool {
	return r.X1 <= r2.X2 &&
		r.X2 >= r2.X1 &&
		r.Y1 <= r2.Y2 &&
		r.Y2 >= r2.Y1
}
