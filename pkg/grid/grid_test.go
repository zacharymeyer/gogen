/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package grid

import (
	"testing"
)

func TestNewRectangle(t *testing.T) {
	rect := NewRectangle(10, 10, 10, 10)
	if rect != nil {
		x1, y1 := rect.X1, rect.Y1
		x2, y2 := rect.X2, rect.Y2
		if x1 != 10 {
			t.Errorf("x1 == %d; expected 10", x1)
		}
		if y1 != 10 {
			t.Errorf("y1 == %d; expected 10", y1)
		}
		if x2 != 20 {
			t.Errorf("x2 == %d; expected 20", x2)
		}
		if y2 != 20 {
			t.Errorf("y2 == %d; expected 20", y2)
		}
	} else {
		t.Errorf("rect is nil")
	}
}

func TestIntersects(t *testing.T) {
	r1 := NewRectangle(10, 10, 10, 10)
	r2 := NewRectangle(5, 10, 20, 20)
	if !r1.Intersects(r2) {
		t.Errorf("r1 does not intersect r2; expected true")
	}
}

func TestCenter(t *testing.T) {
	rect := NewRectangle(0, 0, 9, 9)
	center := rect.Center()
	if center.X != 5 && center.Y != 5 {
		t.Errorf("center == %d, %d; expected 5, 5", center.X, center.Y)
	}
}
