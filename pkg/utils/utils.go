/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package utils

import (
	"math/rand"
	"time"
)

func GetRandomUInt(min, max uint) uint {
	rand.Seed(time.Now().UnixNano())
	return min + uint(rand.Intn(int(max-min)))
}
