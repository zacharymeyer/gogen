/*
Copyright © 2022 Zachary Meyer <zmeyer1@protonmail.com>

*/
package utils

import "testing"

func TestGetRandomUint(t *testing.T) {
	min, max := 10, 100
	for i := 0; i < 100; i++ {
		val := GetRandomUInt(uint(min), uint(max))
		if val < 10 && val > 100 {
			t.Errorf("val == %d, expected between 10-100", val)
		}
	}
}
